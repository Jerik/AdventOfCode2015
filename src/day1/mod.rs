use helper;

fn get_basement_position(directions: &str) -> Option<usize> {
    let mut floor = 0;

    for (i, ch) in directions.char_indices() {
        floor += match ch {
            '(' =>  1,
            ')' => -1,
            _   =>  0,
        };

        if floor == -1 {
            return Some(i+1);
        }
    }

    None
}

pub fn both_parts() {
    match helper::InputFile::read("src/day1/input1.txt") {
        Err(e) => println!("Error: {}", e),
        Ok(input_file) => {
            let directions = input_file.get_content();

            let floor = directions.matches('(').count() - directions.matches(')').count();
            println!("Floor: {}", floor);

            println!("Basement position: {}", get_basement_position(&directions).unwrap());
        },
    }
}