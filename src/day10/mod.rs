fn get_next_sequence(sequence: &[u32]) -> Vec<u32> {
    let modified_sequence: Vec<u32> = sequence.iter()
                                              .chain(&[sequence.last().unwrap() + 1])
                                              .cloned()
                                              .collect();

    let mut new_sequence = Vec::new();
    let mut counter = 0;

    for (i, pair) in modified_sequence.windows(2).enumerate() {
        counter += 1;

        if pair[0] != pair[1] {
            new_sequence.push(counter);
            new_sequence.push(sequence[i]);
            counter = 0;
        }
    }

    new_sequence
}

pub fn both_parts() {
    let mut sequence = vec![1, 1, 1, 3, 1, 2, 2, 1, 1, 3];

    for x in 1..51 {
        sequence = get_next_sequence(&sequence);

        if x == 40 {
            println!("Length of 40th sequence: {}", sequence.len());
        }
    }

    println!("Length of 50th sequence: {}", sequence.len());
}