use helper;
use std::fmt;
use std::str::FromStr;
use std::string::ToString;

struct NonAlphabetError;

impl fmt::Display for NonAlphabetError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Input string contains non-alphabetical or uppercase characters")
    }
}

struct Password {
    alphabet_indices: Vec<usize>,
}

impl Password {
    fn increment(&mut self) {
        for i in self.alphabet_indices.iter_mut().rev() {
            *i = (*i + 1) % 26;

            if *i != 0 {
                break;
            }
        }
    }

    fn is_valid(&self) -> bool {
        let condition1 = self.alphabet_indices.windows(3).any(|triplet| {
            triplet[1] == triplet[0] + 1 && triplet[2] == triplet[1] + 1
        });

        let pair_indices: Vec<usize> = self.alphabet_indices.windows(2).enumerate().filter_map(|(i, pair)| {
            if pair[0] == pair[1] { Some(i) } else { None }
        }).collect();

        let condition2 = if pair_indices.len() > 1 {
            pair_indices.iter().max().unwrap() - pair_indices.iter().min().unwrap() > 1
        } else {
            false
        };

        let condition3 = !([8, 11, 14].iter().any(|x| self.alphabet_indices.contains(x)));

        condition1 && condition2 && condition3
    }
}

impl FromStr for Password {
    type Err = NonAlphabetError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut alphabet_indices = Vec::new();

        for ch in s.chars() {
            let i = helper::ALPHABET.find(ch).ok_or(NonAlphabetError)?;
            alphabet_indices.push(i);
        }

        Ok(Password { alphabet_indices })
    }
}

impl ToString for Password {
    fn to_string(&self) -> String {
        self.alphabet_indices.iter().map(|&i| &helper::ALPHABET[i..i+1]).collect()
    }
}

pub fn both_parts() {
    match Password::from_str("hepxcrrq") {
        Err(e) => println!("Error: {}", e),
        Ok(mut pw) => {
            for x in 1..3 {
                pw.increment();

                while !pw.is_valid() {
                    pw.increment();
                }

                println!("Next password (part {}): {}", x, pw.to_string());
            }
        },
    }
}