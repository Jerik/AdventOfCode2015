use helper;
use serde_json::{self, Value};

fn flattened(value: &Value, ignore_red: bool) -> Vec<Value> {
    match *value {
        Value::Null => vec![],
        Value::Number(_) | Value::Bool(_) | Value::String(_) => vec![value.clone()],
        Value::Array(ref vec) => vec.iter().flat_map(|val| flattened(val, ignore_red)).collect(),
        Value::Object(ref obj) => {
            if ignore_red && obj.values().any(|val| match *val {
                Value::String(ref s) if s == "red" => true,
                _ => false,
            }) {
                vec![]
            } else {
                obj.values().flat_map(|val| flattened(val, ignore_red)).collect()
            }
        },
    }
}

pub fn both_parts() {
    match helper::InputFile::read("src/day12/input12.json") {
        Err(e) => println!("Error: {}", e),
        Ok(input_file) => {
            match serde_json::from_str(&input_file.get_content()) {
                Err(e) => println!("Error: {}", e),
                Ok(value) => {
                    println!("Sum: {}", flattened(&value, false).iter().filter_map(Value::as_i64).sum::<i64>());
                    println!("Sum without red: {}", flattened(&value, true).iter().filter_map(Value::as_i64).sum::<i64>());
                },
            }
        },
    }
}