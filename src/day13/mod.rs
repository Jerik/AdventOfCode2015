use helper;
use std::collections::HashMap;

struct HappinessTable<'a>(HashMap<&'a str, HashMap<&'a str, i32>>);

impl<'a> HappinessTable<'a> {
    fn new() -> HappinessTable<'a> {
        HappinessTable(HashMap::new())
    }

    fn get_highest_total_happiness_change(&self) -> i32 {
        let names: Vec<&str> = self.0.keys().cloned().collect();

        helper::permutations(&names).iter().map(|perm| {
            perm.iter().chain(&perm[0..1]).cloned().collect::<Vec<&str>>().windows(2).map(|pair| {
                self.0[pair[0]][pair[1]] + self.0[pair[1]][pair[0]]
            }).sum::<i32>()
        }).max().unwrap()
    }

    fn set_happiness_value(&mut self, person: &'a str, neighbour: &'a str, value: i32) {
        self.0.entry(person).or_insert_with(HashMap::new).insert(neighbour, value);
    }

    fn add_person(&mut self, name: &'a str) {
        let names: Vec<&str> = self.0.keys().cloned().collect();

        if !names.contains(&name) {
            for happiness_values in self.0.values_mut() {
                happiness_values.insert(name, 0);
            }

            self.0.insert(name, names.iter().map(|&key| (key, 0)).collect());
        }
    }
}

pub fn both_parts() {
    match helper::InputFile::read("src/day13/input13.txt") {
        Err(e) => println!("Error: {}", e),
        Ok(input_file) => {
            let lines = input_file.get_content_as_lines();

            let mut ht = HappinessTable::new();

            for line in &lines {
                if !line.is_empty() {
                    let parts: Vec<&str> = line.split_whitespace().collect();

                    let person = parts.first().unwrap();
                    let neighbour = parts.last().unwrap().trim_right_matches('.');
                    let value = parts[3].parse::<i32>().unwrap() * if parts[2] == "lose" { -1 } else { 1 };

                    ht.set_happiness_value(person, neighbour, value);
                }
            }

            println!("Highest total change in happiness: {}", ht.get_highest_total_happiness_change());

            ht.add_person("me");

            println!("Highest total change in happiness with me: {}", ht.get_highest_total_happiness_change());
        },
    }
}