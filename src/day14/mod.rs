use helper;
use std::collections::HashMap;

#[derive(PartialEq, Eq, Hash)]
struct Reindeer {
    ts: u32,        // top speed in km/s
    fd: u32,        // flying duration in s
    rd: u32,        // resting duration in s
}

impl Reindeer {
    fn get_distance(&self, seconds: u32) -> u32 {
        let num_of_completed_cycles = seconds / (self.fd + self.rd);
        let second_surplus = seconds % (self.fd + self.rd);

        let distance1 = num_of_completed_cycles * (self.ts * self.fd);
        let distance2 = if second_surplus >= self.fd { self.ts * self.fd } else { self.ts * second_surplus };

        distance1 + distance2
    }
}

pub fn both_parts() {
    match helper::InputFile::read("src/day14/input14.txt") {
        Err(e) => println!("Error: {}", e),
        Ok(input_file) => {
            let lines = input_file.get_content_as_lines();

            let mut reindeers: Vec<Reindeer> = Vec::new();
            let mut points: HashMap<&Reindeer, u32> = HashMap::new();

            for line in &lines {
                let params: Vec<u32> = line.split_whitespace().filter_map(|s| s.parse::<u32>().ok()).collect();

                if params.len() == 3 {
                    reindeers.push(Reindeer{ ts: params[0], fd: params[1], rd: params[2] });
                }
            }

            for i in 1..2504 {
                let distances: HashMap<&Reindeer, u32> = reindeers.iter().map(|reindeer| {
                    (reindeer, reindeer.get_distance(i))
                }).collect();
                
                let max_distance = distances.values().max().unwrap();
                
                for reindeer in &reindeers {
                    if distances[reindeer] == *max_distance {
                        *points.entry(reindeer).or_insert(0) += 1;
                    }
                }

                if i == 2503 {
                    println!("Highest distance: {}", max_distance);
                }
            }

             println!("Highest points: {}", points.values().max().unwrap());
        },
    }
}