use helper;

struct Ingredient {
    capacity: i32,
    durability: i32,
    flavour: i32,
    texture: i32,
    calories: i32,
}

impl Ingredient {
    fn new(properties: &[i32]) -> Option<Ingredient> {
        if properties.len() == 5 {
            Some(Ingredient {
                capacity: properties[0],
                durability: properties[1],
                flavour: properties[2],
                texture: properties[3],
                calories: properties[4],
            })
        } else {
            None
        }
    }
}

pub fn both_parts() {
    match helper::InputFile::read("src/day15/input15.txt") {
        Err(e) => println!("Error: {}", e),
        Ok(input_file) => {
            let lines = input_file.get_content_as_lines();

            let ingredients: Vec<Ingredient> = lines.iter().filter_map(|line| {
                Ingredient::new(
                    &line.split(|ch| ch == ' ' || ch == ',')
                         .filter_map(|s| s.parse::<i32>().ok())
                         .collect::<Vec<i32>>()
                )
            }).collect();

            let scores: Vec<(i32, bool)> = helper::nwps(100, ingredients.len()).iter().map(|comb| {
                let cookie = ingredients.iter().enumerate().fold([0; 5], |sums, (i, ing)| {
                    [sums[0] + comb[i] as i32 * ing.capacity,
                    sums[1] + comb[i] as i32 * ing.durability,
                    sums[2] + comb[i] as i32 * ing.flavour,
                    sums[3] + comb[i] as i32 * ing.texture,
                    sums[4] + comb[i] as i32 * ing.calories]
                });
                
                (cookie[0..4].iter().map(|&x| if x < 0 { 0 } else { x }).product(), cookie[4] == 500)
            }).collect();

            println!("Highest cookie score: {}", scores.iter().map(|&(x, _)| x).max().unwrap());
            println!("Highest cookie score with 500 calories: {}", scores.iter().filter_map(|&(x, b)| {
                if b { Some(x) } else { None }
            }).max().unwrap());
        }
    }
}