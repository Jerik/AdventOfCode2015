use helper;
use std::collections::HashMap;

struct Sue<'a> {
    amounts: HashMap<&'a str, u32>,
}

impl<'a> Sue<'a> {
    fn check(&self, use_ranges: bool) -> bool {
        if let Some(&num) = self.amounts.get("children") {
            if num != 3 { return false; }
        }

        if let Some(&num) = self.amounts.get("cats") {
            if !use_ranges && num != 7 || use_ranges && num <= 7 { return false; }
        }

        if let Some(&num) = self.amounts.get("samoyeds") {
            if num != 2 { return false; }
        }

        if let Some(&num) = self.amounts.get("pomeranians") {
            if !use_ranges && num != 3 || use_ranges && num >= 3 { return false; }
        }

        if let Some(&num) = self.amounts.get("akitas") {
            if num != 0 { return false; }
        }

        if let Some(&num) = self.amounts.get("viszlas") {
            if num != 0 { return false; }
        }

        if let Some(&num) = self.amounts.get("goldfish") {
            if !use_ranges && num != 5 || use_ranges && num >= 5 { return false; }
        }

        if let Some(&num) = self.amounts.get("trees") {
            if !use_ranges && num != 3 || use_ranges && num <= 3 { return false; }
        }

        if let Some(&num) = self.amounts.get("cars") {
            if num != 2 { return false; }
        }

        if let Some(&num) = self.amounts.get("perfumes") {
            if num != 1 { return false; }
        }

        true
    }
}

pub fn both_parts() {
    match helper::InputFile::read("src/day16/input16.txt") {
        Err(e) => println!("Error: {}", e),
        Ok(input_file) => {
            let lines = input_file.get_content_as_lines();

            let sues: Vec<Sue> = lines.iter().filter_map(|line| {
                let (nums, names): (Vec<&str>, Vec<&str>) = line.split(|ch| ch == ' ' || ch == ',' || ch == ':')
                    .skip(2).filter(|s| !s.is_empty()).partition(|s| s.contains(char::is_numeric));

                if names.len() == nums.len() {
                    let amounts = names.into_iter().zip(nums.iter().filter_map(|s| {
                        s.parse::<u32>().ok()
                    })).collect();

                    Some(Sue { amounts })
                } else {
                    None
                }
            }).collect();

            for &(use_ranges, x) in &[(false, 1), (true, 2)] {
                for (i, sue) in sues.iter().enumerate() {
                    if sue.check(use_ranges) {
                        println!("It was Sue no. {} (part {}).", i+1, x);
                        break;
                    }
                }
            }
        },
    }
}