use helper;

pub fn both_parts() {
    match helper::InputFile::read("src/day17/input17.txt") {
        Err(e) => println!("Error: {}", e),
        Ok(input_file) => {
            let lines = input_file.get_content_as_lines();

            let container_sizes: Vec<u32> = lines.iter()
                                                 .filter_map(|line| line.parse::<u32>().ok())
                                                 .collect();

            let container_combinations: Vec<Vec<u32>> = helper::combinations(&container_sizes).iter().filter(|comb| {
                comb.iter().sum::<u32>() == 150
            }).cloned().collect();

            println!("Number of combinations: {}", container_combinations.len());

            let minimum_number = container_combinations.iter().map(|comb| comb.len()).min().unwrap();

            println!("Number of combinations with minimal number of containers: {}",
                     container_combinations.iter().filter(|comb| comb.len() == minimum_number).count());
        },
    }
}