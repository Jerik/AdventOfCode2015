use helper;
use std::io;
use std::collections::HashMap;

#[derive(Clone, PartialEq, Eq, Hash)]
struct Position {
    x: usize,
    y: usize,
}

#[derive(Clone, PartialEq)]
enum LightStatus {
    TurnedOn,
    TurnedOff,
}

#[derive(Clone)]
struct LightGrid{
    grid: HashMap<Position, LightStatus>,
    width: usize,
    height: usize,
}

impl LightGrid {
    fn new(width: usize, height: usize) -> LightGrid {
        let mut grid = HashMap::new();

        for x in 0 .. width + 2 {
            for y in 0 .. height + 2 {
                grid.insert(Position { x, y }, LightStatus::TurnedOff);
            }
        }

        LightGrid { grid, width, height }
    }

    fn turn_light_on(&mut self, pos: Position) {
        self.grid.insert(pos, LightStatus::TurnedOn);
    }

    fn turn_light_off(&mut self, pos: Position) {
        self.grid.insert(pos, LightStatus::TurnedOff);
    }

    fn turn_corners_on(&mut self) {
        for &(x, y) in &[(1, 1), (self.width, 1), (1, self.height), (self.width, self.height)] {
            self.grid.insert(Position { x, y }, LightStatus::TurnedOn);
        }
    }

    fn num_of_lit_neighbours(&self, pos: &Position) -> usize {
        let mut num = 0;

        for x in pos.x - 1 .. pos.x + 2 {
            for y in pos.y - 1 .. pos.y + 2 {
                if (x != pos.x || y != pos.y) && self.grid[&Position { x, y }] == LightStatus::TurnedOn {
                    num += 1;
                }
            }
        }

        num
    }

    fn num_of_lit_lights(&self) -> usize {
        self.grid.values().filter(|v| **v == LightStatus::TurnedOn).count()
    }

    fn update(&mut self) {
        let current_grid = self.clone();

        for x in 1 .. self.width + 1 {
            for y in 1 .. self.height + 1 {
                let pos = Position { x, y };
                let current_light_status = &current_grid.grid[&pos];

                match (current_light_status, current_grid.num_of_lit_neighbours(&pos)) {
                    (_, num) if num == 3 => self.turn_light_on(pos),
                    (&LightStatus::TurnedOn, num) if num == 2 => {},
                    _ => self.turn_light_off(pos),
                }
            }
        }
    }
}

fn initialize_grid() -> io::Result<LightGrid> {
    let mut light_grid = LightGrid::new(100, 100);

    let input_file = helper::InputFile::read("src/day18/input18.txt")?;
    let lines = input_file.get_content_as_lines();

    lines.iter().filter(|line| !line.is_empty()).enumerate().for_each(|(i, line)| {
        line.char_indices().for_each(|(j, ch)| {
            if let '#' = ch {
                light_grid.turn_light_on(Position { x: i+1, y: j+1 });
            }
        })
    });

    Ok(light_grid)
}

pub fn both_parts() {
    match initialize_grid() {
        Err(e) => println!("Error: {}", e),
        Ok(mut light_grid) => {
            for _ in 0..100 {
                light_grid.update();
            }

            println!("Number of lit lights: {}", light_grid.num_of_lit_lights());
        },
    }

    match initialize_grid() {
        Err(e) => println!("Error: {}", e),
        Ok(mut light_grid) => {
            light_grid.turn_corners_on();

            for _ in 0..100 {
                light_grid.update();
                light_grid.turn_corners_on();
            }

            println!("Number of lit lights with corners on: {}", light_grid.num_of_lit_lights());
        },
    }
}