use helper;
use std::collections::HashSet;

struct Replacement<'a> {
    to_replace: &'a str,
    to_insert: &'a str,
}

struct Calibration<'a> {
    molecule: &'a str,
    replacements: Vec<Replacement<'a>>,
}

impl<'a> Calibration<'a> {
    fn num_of_molecules(&self) -> usize {
        let mut molecules: HashSet<String> = HashSet::new();

        for repl in &self.replacements {
            let mut head = String::from("");
            let mut tail = &self.molecule[..];

            while let Some(i) = tail.find(repl.to_replace) {
                let modified_tail = tail.replacen(repl.to_replace, repl.to_insert, 1);
                molecules.insert(head.clone() + &modified_tail);

                let split = head.len() + i + repl.to_replace.len();
                head = String::from(&self.molecule[..split]);
                tail = &self.molecule[split..];
            }
        }

        molecules.len()
    } 
}

pub fn both_parts() {
    match helper::InputFile::read("src/day19/input19.txt") {
        Err(e) => println!("Error: {}", e),
        Ok(input_file) => {
            let lines = input_file.get_content_as_lines();

            let replacements: Vec<Replacement> = lines.iter().filter_map(|line| {
                let substrings: Vec<&str> = line.split_whitespace().collect();
                
                if substrings.len() == 3 && substrings[1] == "=>" {
                    Some(Replacement{ to_replace: substrings[0], to_insert: substrings[2] })
                } else {
                    None
                }
            }).collect();

            let molecule = lines.iter().filter(|line| !line.is_empty()).last().unwrap();

            let calibration = Calibration { molecule, replacements };

            println!("Number of createable molecules: {}", calibration.num_of_molecules());

            let steps = molecule.chars().filter(|ch| ch.is_uppercase()).count()
                        - helper::count_pattern(molecule, "Rn")
                        - helper::count_pattern(molecule, "Ar")
                        - 2 * helper::count_pattern(molecule, "Y")
                        - 1;

            println!("Number of replacement steps: {}", steps);
        },
    }
}