use helper;

struct Present {
    l: u32,
    w: u32,
    h: u32,
}

impl Present {
    fn get_smallest_dimensions(&self) -> (u32, u32) {
        let mut dimensions = vec![self.l, self.w, self.h];
        dimensions.sort();

        (dimensions[0], dimensions[1])
    }

    fn get_paper_amount(&self) -> u32 {
        let surface_area = 2 * self.l * self.w + 2 * self.l * self.h + 2 * self.w * self.h;

        let (small_dim1, small_dim2) = self.get_smallest_dimensions();
        let extra_amount = small_dim1 * small_dim2;

        surface_area + extra_amount
    }

    fn get_ribbon_length(&self) -> u32 {
        let bow = self.l * self.w * self.h;

        let (small_dim1, small_dim2) = self.get_smallest_dimensions();
        let perimeter = 2 * small_dim1 + 2 * small_dim2;

        perimeter + bow
    }
}

pub fn both_parts() {
    match helper::InputFile::read("src/day2/input2.txt") {
        Err(e) => println!("Error: {}", e),
        Ok(input_file) => {
            let lines = input_file.get_content_as_lines();

            let mut total_paper_amount = 0;
            let mut total_ribbon_length = 0;

            for line in &lines {
                let dimensions: Vec<u32> = line.split('x')
                                               .filter_map(|s| s.parse::<u32>().ok())
                                               .collect();

                if dimensions.len() == 3 {
                    let present = Present { l: dimensions[0], w: dimensions[1], h: dimensions[2] };

                    total_paper_amount += present.get_paper_amount();
                    total_ribbon_length += present.get_ribbon_length();
                }
            }

            println!("Square feets of wrapping paper: {}", total_paper_amount);
            println!("Ribbon length: {}", total_ribbon_length);
        },
    }
}