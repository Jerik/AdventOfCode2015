#[allow(unreadable_literal)]
pub fn both_parts() {
    let input = 33100000;
    let limit = input / 10;

    let house_no_part1 = (1..limit).fold(vec![0; limit], |mut houses, elf| {
        let mut i = elf;

        while i < limit {
            houses[i] += elf * 10;
            i += elf;
        }

        houses
    }).iter().position(|&v| v >= input).unwrap();
    
    println!("Lowest house number (part 1): {}", house_no_part1);

    let house_no_part2 = (1..limit).fold(vec![0; limit], |mut houses, elf| {
        let mut i = elf;

        for _ in 0..50 {
            if i < limit {
                houses[i] += elf * 11;
                i += elf;
            } else {
                break;
            }
        }

        houses
    }).iter().position(|&v| v >= input).unwrap();

    println!("Lowest house number (part 2): {}", house_no_part2);
}