use helper;
use std::cmp::Ordering;

#[derive(PartialEq)]
struct Item {
    cost: u32,
    damage: u32,
    armor: u32,
}

impl Item {
    fn new(cost: u32, damage: u32, armor: u32) -> Item {
        Item { cost, damage, armor }
    }
}

trait Fighter {
    fn get_hit_points(&self) -> u32;

    fn get_damage_stat(&self) -> u32;

    fn get_armor_stat(&self) -> u32;

    fn fight<T: Fighter>(&self, enemy: &T) -> bool {
        let mut hp = self.get_hit_points();
        let mut enemy_hp = enemy.get_hit_points();

        let atk_dmg = match self.get_damage_stat().cmp(&enemy.get_armor_stat()) {
            Ordering::Greater => self.get_damage_stat() - enemy.get_armor_stat(),
            _ => 1,
        };

        let enemy_atk_dmg = match enemy.get_damage_stat().cmp(&self.get_armor_stat()) {
            Ordering::Greater => enemy.get_damage_stat() - self.get_armor_stat(),
            _ => 1,
        };

        loop {
            match atk_dmg.cmp(&enemy_hp) {
                Ordering::Less => enemy_hp -= atk_dmg,
                _ => return true,
            }

            match enemy_atk_dmg.cmp(&hp) {
                Ordering::Less => hp -= enemy_atk_dmg,
                _ => return false,
            }
        }
    }
}

struct Player<'a> {
    weapon: &'a Item,
    armor: &'a Option<Item>,
    ring1: &'a Option<Item>,
    ring2: &'a Option<Item>,
}

impl<'a> Player<'a> {
    fn get_expense(&self) -> u32 {
        self.weapon.cost + match *self.armor {
            Some(ref armor) => armor.cost,
            None => 0,
        } + match *self.ring1 {
            Some(ref ring) => ring.cost,
            None => 0,
        } + match *self.ring2 {
            Some(ref ring) => ring.cost,
            None => 0,
        }
    }
}

impl<'a> Fighter for Player<'a> {
    fn get_hit_points(&self) -> u32 {
        100
    }

    fn get_damage_stat(&self) -> u32 {
        self.weapon.damage + match *self.ring1 {
            Some(ref ring) => ring.damage,
            None => 0,
        } + match *self.ring2 {
            Some(ref ring) => ring.damage,
            None => 0,
        }
    }

    fn get_armor_stat(&self) -> u32 {
        (match *self.armor {
            Some(ref armor) => armor.armor,
            None => 0,
        }) + match *self.ring1 {
            Some(ref ring) => ring.armor,
            None => 0,
        } + match *self.ring2 {
            Some(ref ring) => ring.armor,
            None => 0,
        }
    }
}

struct Boss {
    hit_points: u32,
    damage_stat: u32,
    armor_stat: u32,
}

impl Fighter for Boss {
    fn get_hit_points(&self) -> u32 {
        self.hit_points
    }

    fn get_armor_stat(&self) -> u32 {
        self.armor_stat
    }

    fn get_damage_stat(&self) -> u32 {
        self.damage_stat
    }
}

pub fn both_parts() {
    match helper::InputFile::read("src/day21/input21.txt") {
        Err(e) => println!("Error: {}", e),
        Ok(input_file) => {
            let lines = input_file.get_content_as_lines();

            let boss_stats: Vec<u32> = lines.iter().flat_map(|line| {
                line.split_whitespace().filter_map(|x| x.parse::<u32>().ok())
            }).collect();

            if boss_stats.len() == 3 {
                let boss = Boss {
                    hit_points: boss_stats[0],
                    damage_stat: boss_stats[1],
                    armor_stat: boss_stats[2]
                };

                let weapons = [
                    Item::new(8, 4, 0),
                    Item::new(10, 5, 0),
                    Item::new(25, 6, 0),
                    Item::new(40, 7, 0),
                    Item::new(74, 8, 0),
                ];

                let armors = [
                    Some(Item::new(13, 0, 1)),
                    Some(Item::new(31, 0, 2)),
                    Some(Item::new(53, 0, 3)),
                    Some(Item::new(75, 0, 4)),
                    Some(Item::new(102, 0, 5)),
                    None,
                ];

                let rings = [
                    Some(Item::new(25, 1, 0)),
                    Some(Item::new(50, 2, 0)),
                    Some(Item::new(100, 3, 0)),
                    Some(Item::new(20, 0, 1)),
                    Some(Item::new(40, 0, 2)),
                    Some(Item::new(80, 0, 3)),
                    None,
                ];

                let mut player_setups: Vec<Player> = Vec::new();

                for weapon in &weapons {
                    for armor in &armors {
                        for ring1 in &rings {
                            rings.iter().filter(|&ring2| match (ring1, ring2) {
                                (&Some(ref r1), &Some(ref r2)) => r1 != r2,
                                _ => true,
                            }).for_each(|ring2| {
                                player_setups.push(Player{ weapon, armor, ring1, ring2 });
                            })
                        }
                    }
                }

                player_setups.sort_unstable_by(|p1, p2| p1.get_expense().cmp(&p2.get_expense()));

                let winner = player_setups.iter().position(|p| p.fight(&boss)).unwrap();
                println!("Least gold for win: {}", player_setups[winner].get_expense());

                let loser = player_setups.iter().rposition(|p| !p.fight(&boss)).unwrap();
                println!("Most gold for defeat: {}", player_setups[loser].get_expense());
            }
        },
    }
}