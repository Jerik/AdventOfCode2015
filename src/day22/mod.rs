use helper;
use std::cmp::Ordering;
use std::collections::VecDeque;

#[derive(PartialEq)]
enum Effect {
    Shield,
    Poison,
    Recharge,
}

struct Spell {
    mana_cost: u32,
    damage: i32,
    heal: i32,
    effect: Option<Effect>,
}

const SPELLS: [Spell; 5] = [
    Spell { mana_cost: 53, damage: 4, heal: 0, effect: None },
    Spell { mana_cost: 73, damage: 2, heal: 2, effect: None },
    Spell { mana_cost: 113, damage: 0, heal: 0, effect: Some(Effect::Shield) },
    Spell { mana_cost: 173, damage: 0, heal: 0, effect: Some(Effect::Poison) },
    Spell { mana_cost: 229, damage: 0, heal: 0, effect: Some(Effect::Recharge) },
];

struct Player {
    hit_points: i32,
    armor: i32,
    mana: u32,
    spent_mana: u32,
}

impl Player {
    fn new(hit_points: i32, mana: u32, spent_mana: u32) -> Player {
        Player { hit_points, armor: 0, mana, spent_mana }
    }
}

struct State<'a> {
    player: Player,
    boss_hp: i32,
    active_effects: Vec<(&'a Effect, u32)>,
    is_player_turn: bool,
}

impl<'a> State<'a> {
    fn new(player: Player, boss_hp: i32, active_effects: Vec<(&'a Effect, u32)>, is_player_turn: bool) -> State<'a> {
        State { player, boss_hp, active_effects, is_player_turn }
    }

    fn init_and_check(&mut self, part2: bool) -> bool {
        if part2 && self.is_player_turn {
            self.player.hit_points -= 1;
        }

        for eff in &self.active_effects {
            match *eff.0 {
                Effect::Poison => self.boss_hp -= 3,
                Effect::Recharge => self.player.mana += 101,
                Effect::Shield => self.player.armor = 7,
            }
        }
        
        self.boss_hp <= 0
    }

    fn get_active_effects_of_child(&self, new_effect: &'a Option<Effect>) -> Vec<(&'a Effect, u32)> {
        let new_effect_slice: &[(&Effect, u32)] = match *new_effect {
            Some(Effect::Shield) => &[(&Effect::Shield, 6)],
            Some(Effect::Poison) => &[(&Effect::Poison, 6)],
            Some(Effect::Recharge) => &[(&Effect::Recharge, 5)],
            None => &[],
        };

        self.active_effects.iter().chain(new_effect_slice).filter_map(|&(eff, timer)| {
            if timer > 0 {
                Some((eff, timer-1))
            } else {
                None
            }
        }).collect()
    }

    fn get_player_turn_child_states(&self) -> Vec<State<'a>> {
        let mut child_states = Vec::new();

        for spell in &SPELLS {
            if spell.mana_cost > self.player.mana {
                break;
            } else {
                if let Some(ref eff) = spell.effect {
                    let num_of_already_active = self.active_effects.iter().filter(|&&(act_eff, timer)| {
                        act_eff == eff && timer > 0
                    }).count();

                    if num_of_already_active > 0 {
                        continue;
                    }
                }

                let child = State::new(
                    Player::new(
                        self.player.hit_points + spell.heal,
                        self.player.mana - spell.mana_cost,
                        self.player.spent_mana + spell.mana_cost
                    ),
                    self.boss_hp - spell.damage,
                    self.get_active_effects_of_child(&spell.effect),
                    !self.is_player_turn
                );

                child_states.push(child);
            }
        }

        child_states
    }

    fn get_boss_turn_child_states(&self, boss_damage: i32, part2: bool) -> Vec<State<'a>> {
        let mut child_states = Vec::new();

        let new_player_hp = self.player.hit_points - match boss_damage.cmp(&self.player.armor) {
            Ordering::Greater => boss_damage - self.player.armor,
            _ => 1,
        };

        if !part2 && new_player_hp > 0 || part2 && new_player_hp > 1 {
            let child = State::new(
                Player::new(
                    new_player_hp,
                    self.player.mana,
                    self.player.spent_mana
                ),
                self.boss_hp,
                self.get_active_effects_of_child(&None),
                !self.is_player_turn
            );

            child_states.push(child);
        }

        child_states
    }

    fn get_child_states(self, boss_damage: i32, part2: bool) -> Vec<State<'a>> {
        if self.is_player_turn {
            self.get_player_turn_child_states()
        } else {
            self.get_boss_turn_child_states(boss_damage, part2)
        }
    } 
}

fn bfs(boss_hp: i32, boss_damage: i32, part2: bool) -> Option<u32> {
    let initial_state = State::new(Player::new(50, 500, 0), boss_hp, vec![], true);
    let mut queue = VecDeque::new();
    queue.push_back(initial_state);

    while let Some(mut current) = queue.pop_front() {
        if current.init_and_check(part2) {
            return Some(current.player.spent_mana);
        } else {
            for child in current.get_child_states(boss_damage, part2) {
                queue.push_back(child);
            }
        }
    }

    None
}

pub fn both_parts() {
    match helper::InputFile::read("src/day22/input22.txt") {
        Err(e) => println!("Error: {}", e),
        Ok(input_file) => {
            let lines = input_file.get_content_as_lines();

            let boss_stats: Vec<i32> = lines.iter().flat_map(|line| {
                line.split_whitespace().filter_map(|x| x.parse::<i32>().ok())
            }).collect();

            if boss_stats.len() == 2 {
                println!("Least mana for win (part 1): {}", bfs(boss_stats[0], boss_stats[1], false).unwrap());
                println!("Least mana for win (part 2): {}", bfs(boss_stats[0], boss_stats[1], true).unwrap());
            }
        },
    }
}