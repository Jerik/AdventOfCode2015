use helper;
use std::collections::HashMap;

enum Instruction<'a> {
    Half(&'a str),
    Triple(&'a str),
    Increment(&'a str),
    Jump(i32),
    JumpIfEven(&'a str, i32),
    JumpIfOne(&'a str, i32),
}

impl<'a> Instruction<'a> {
    fn execute(&self, registers: &mut HashMap<&str, u32>, index: i32) -> i32 {
        match *self {
            Instruction::Half(reg) => *registers.get_mut(reg).unwrap() /= 2,
            Instruction::Triple(reg) => *registers.get_mut(reg).unwrap() *= 3,
            Instruction::Increment(reg) => *registers.get_mut(reg).unwrap() += 1,
            Instruction::Jump(offset) => return index + offset,
            Instruction::JumpIfEven(reg, offset) => {
                if registers[reg] % 2 == 0 {
                    return index + offset;
                }
            },
            Instruction::JumpIfOne(reg, offset) => {
                if registers[reg] == 1 {
                    return index + offset;
                }
            },
        };

        index + 1
    }
}

fn get_register_b_value(instructions: &[Instruction], reg_a_init: u32) -> u32 {
    let mut registers = HashMap::new();
    registers.insert("a", reg_a_init);
    registers.insert("b", 0);

    let mut i: i32 = 0;

    while let Some(instr) = instructions.get(i as usize) {
        i = instr.execute(&mut registers, i);
    }

    registers["b"]
}

pub fn both_parts() {
    match helper::InputFile::read("src/day23/input23.txt") {
        Err(e) => println!("Error: {}", e),
        Ok(input_file) => {
            let lines = input_file.get_content_as_lines();

            let instructions: Vec<Instruction> = lines.iter().filter_map(|line| {
                let substrings: Vec<&str> = line.split_whitespace().map(|s| s.trim_matches(',')).collect();

                if !substrings.is_empty() {
                    match substrings[0] {
                        "hlf" => Some(Instruction::Half(substrings[1])),
                        "tpl" => Some(Instruction::Triple(substrings[1])),
                        "inc" => Some(Instruction::Increment(substrings[1])),
                        "jmp" => Some(Instruction::Jump(substrings[1].parse().unwrap())),
                        "jie" => Some(Instruction::JumpIfEven(substrings[1], substrings[2].parse().unwrap())),
                        "jio" => Some(Instruction::JumpIfOne(substrings[1], substrings[2].parse().unwrap())),
                        _ => None,
                    }
                } else {
                    None
                }
            }).collect();

            println!("Register b (part 1): {}", get_register_b_value(&instructions, 0));
            println!("Register b (part 2): {}", get_register_b_value(&instructions, 1));
        },
    }
}