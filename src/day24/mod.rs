use helper;

fn get_qua_ent(present_weights: &[u64], num_of_compartments: u64) -> Option<u64> {
    get_qua_ent_steps(
        present_weights,
        present_weights.iter().sum::<u64>() / num_of_compartments,
        num_of_compartments - 1,
        true
    )
}

fn get_qua_ent_steps(remaining: &[u64], weight: u64, step: u64, calc: bool) -> Option<u64> {
    if step == 0 {
        Some(if calc { remaining.iter().product() } else { 0 })
    } else {
        for n in 1..remaining.len()+1 {
            for comb in helper::sized_combinations(remaining, n).iter().filter(|comb| {
                comb.iter().sum::<u64>() == weight
            }) {
                if get_qua_ent_steps(&helper::get_difference(remaining, comb), weight, step-1, false).is_some() {
                    return Some(if calc { comb.iter().product() } else { 0 });
                }
            }
        }

        None
    }
}

pub fn both_parts() {
    match helper::InputFile::read("src/day24/input24.txt") {
        Err(e) => println!("Error: {}", e),
        Ok(input_file) => {
            let lines = input_file.get_content_as_lines();
            let present_weights: Vec<u64> = lines.iter().filter_map(|line| line.parse().ok()).collect();

            println!("Quantum entanglement (part 1): {}", get_qua_ent(&present_weights, 3).unwrap());
            println!("Quantum entanglement (part 2): {}", get_qua_ent(&present_weights, 4).unwrap());
        },
    }
}