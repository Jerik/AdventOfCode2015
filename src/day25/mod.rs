use helper;

#[allow(unreadable_literal)]
fn get_code(code_number: u64) -> u64 {
    (1..code_number).fold(20151125, |code, _| {
        (code * 252533) % 33554393
    })
}

fn calculate_code_number(row: u64, col: u64) -> u64 {
    ((col+row-2) * (col+row-1)) / 2 + col
}

pub fn part1() {
    match helper::InputFile::read("src/day25/input25.txt") {
        Err(e) => println!("Error: {}", e),
        Ok(input_file) => {
            let input = input_file.get_content();

            let grid_position: Vec<u64> = input.split_whitespace().filter_map(|s| {
                s.trim_matches(|ch| ch == ',' || ch == '.').parse().ok()
            }).collect();

            if grid_position.len() == 2 {
                println!("Code: {}", get_code(calculate_code_number(grid_position[0], grid_position[1])));
            }
        },
    }
}