use helper;

#[derive(Clone, Copy, PartialEq)]
struct Position {
    x: i32,
    y: i32,
}

fn get_visited_positions(directions: &str) -> Vec<Position> {
    let mut current_pos = Position { x: 0, y: 0 };
    let mut visited = vec![current_pos];

    for ch in directions.chars() {
        match ch {
            '^' => current_pos.y += 1,
            'v' => current_pos.y -= 1,
            '>' => current_pos.x += 1,
            '<' => current_pos.x -= 1,
            _ => {},
        }

        if !visited.contains(&current_pos) {
            visited.push(current_pos);
        }
    }

    visited
}

pub fn both_parts() {
    match helper::InputFile::read("src/day3/input3.txt") {
        Err(e) => println!("Error: {}", e),
        Ok(input_file) => {
            let directions = input_file.get_content();

            println!("{} houses receive at least one present (part 1).", get_visited_positions(&directions).len());

            let santa_dir: String = directions.char_indices().filter(|&(i, _)| i % 2 == 0).map(|(_, ch)| ch).collect();
            let robo_dir : String = directions.char_indices().filter(|&(i, _)| i % 2 != 0).map(|(_, ch)| ch).collect();

            let visited1 = get_visited_positions(&santa_dir);
            let mut visited2 = get_visited_positions(&robo_dir);
            visited2.retain(|pos| !visited1.contains(pos));

            println!("{} houses receive at least one present (part 2).", visited1.len() + visited2.len());
        },
    }
}