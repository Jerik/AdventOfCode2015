use md5;

pub fn both_parts() {
    let mut number = 1;
    let mut part1_completed = false;

    loop {
        let mut key = String::from("iwrupvqb");
        key.push_str(&number.to_string());
        let digest = md5::compute(key);

        if !part1_completed && format!("{:x}", digest).starts_with("00000") {
            println!("Number (part 1): {}", number);
            part1_completed = true;
        }

        if part1_completed && format!("{:x}", digest).starts_with("000000") {
            println!("Number (part 2): {}", number);
            break;
        }
        
        number += 1;
    }
}