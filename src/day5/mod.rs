use helper;
use std::collections::HashMap;

fn is_nice_part1(string: &str) -> bool {
    "aeiou".chars().map(|ch| string.matches(ch).count()).sum::<usize>() >= 3 &&
    helper::ALPHABET.chars().any(|ch| string.contains(&format!("{}{}", ch, ch))) &&
    !(["ab", "cd", "pq", "xy"].iter().any(|s| string.contains(s)))
}

fn is_nice_part2(string: &str) -> bool {
    let chars: Vec<char> = string.chars().collect();
    let pairs: Vec<&[char]> = chars.windows(2).collect();

    let mut pair_counter = HashMap::new();

    for pair in &pairs {
        *pair_counter.entry(pair).or_insert(0) += 1;
    }

    let condition1 = pair_counter.iter().any(|(&pair, &num)| {
        num > 1 && pairs.iter().rposition(|x| x == pair).unwrap() - pairs.iter().position(|x| x == pair).unwrap() > 1
    });

    if condition1 {
        let two_apart = |inds: &[usize]| inds[1] - inds[0] == 2;

        for ch in helper::ALPHABET.chars() {
            let (even_indices, odd_indices): (Vec<usize>, Vec<usize>) = string.match_indices(ch)
                                                                              .map(|(i, _)| i)
                                                                              .partition(|i| i % 2 == 0);

            if even_indices.windows(2).any(&two_apart) || odd_indices.windows(2).any(&two_apart) {
                return true;
            }
        }
    }

    false
}

pub fn both_parts() {
    match helper::InputFile::read("src/day5/input5.txt") {
        Err(e) => println!("Error: {}", e),
        Ok(input_file) => {
            let strings = input_file.get_content_as_lines();
            
            let (nice_strings_part1, nice_strings_part2) = strings.iter().fold((0, 0), |(sum1, sum2), s| {
                (sum1 + if is_nice_part1(s) { 1 } else { 0 },
                sum2 + if is_nice_part2(s) { 1 } else { 0 })
            });

            println!("There are {} nice strings.", nice_strings_part1);
            println!("The above number is ridiculous. Actually, there are {} nice strings.", nice_strings_part2);
        },
    }
}