use helper;
use std::collections::HashMap;

#[derive(PartialEq, Eq, Hash)]
struct Position {
    x: u32,
    y: u32,
}

enum Status {
    TurnedOn,
    TurnedOff,
}

struct Light {
    status: Status,
    brightness: u32,
}

impl Light {
    fn new() -> Light {
        Light {
            status: Status::TurnedOff,
            brightness: 0,
        }
    }

    fn turn_on(&mut self) {
        self.status = Status::TurnedOn;

        self.brightness += 1;
    }

    fn turn_off(&mut self) {
        self.status = Status::TurnedOff;

        if self.brightness > 0 {
            self.brightness -= 1;
        }
    }

    fn toggle(&mut self) {
        self.status = match self.status {
            Status::TurnedOn => Status::TurnedOff,
            Status::TurnedOff => Status::TurnedOn,
        };

        self.brightness += 2;
    }
}

pub fn both_parts() {
    match helper::InputFile::read("src/day6/input6.txt") {
        Err(e) => println!("Error: {}", e),
        Ok(input_file) => {
            let lines = input_file.get_content_as_lines();
            
            let mut light_grid: HashMap<Position, Light> = HashMap::new();

            for line in &lines {
                let coords: Vec<u32> = line.split(|ch| ch == ' ' || ch == ',')
                                           .filter_map(|x| x.parse::<u32>().ok())
                                           .collect();

                if coords.len() == 4 {
                    let corner1 = Position { x: coords[0], y: coords[1] };
                    let corner2 = Position { x: coords[2], y: coords[3] };

                    for x in corner1.x .. corner2.x + 1 {
                        for y in corner1.y .. corner2.y + 1 {
                            let light = light_grid.entry(Position { x, y }).or_insert_with(Light::new);

                            if line.starts_with("turn on") {
                                light.turn_on();
                            } else if line.starts_with("turn off") {
                                light.turn_off();
                            } else if line.starts_with("toggle") {
                                light.toggle();
                            }
                        }
                    }
                }
            }

            println!("Number of lit lights: {}", light_grid.values().filter(|light| match light.status {
                Status::TurnedOn => true, Status::TurnedOff => false,
            }).count());
            println!("Total brightness: {}", light_grid.values().map(|light| light.brightness).sum::<u32>());
        },
    }
}