use helper;
use std::collections::HashMap;

#[derive(PartialEq, Eq, Hash)]
enum Gate {
    Assign,
    And,
    Or,
    Not,
    LShift,
    RShift,
}

#[derive(PartialEq, Eq, Hash)]
enum Signal<'a> {
    Number(u16),
    WireID(&'a str),
}

#[derive(PartialEq, Eq, Hash)]
struct Command<'a> {
    gate: Gate,
    target_wire: &'a str,
    source_signals: Vec<Signal<'a>>,
}

fn get_wire_a_signal(commands: &[Command]) -> u16 {
    let mut wires: HashMap<&str, u16> = HashMap::new();
    let mut executed: HashMap<&Command, bool> = HashMap::new();

    while executed.values().filter(|&&b| b).count() < commands.len() {
        for cmd in commands {
            if !(executed.get(cmd) == Some(&true)) {
                let signal_numbers: Vec<u16> = cmd.source_signals.iter().filter_map(|signal| match *signal {
                    Signal::Number(num) => Some(num),
                    Signal::WireID(id) => if wires.contains_key(id) { Some(wires[id]) } else { None },
                }).collect();

                if signal_numbers.len() == cmd.source_signals.len() {
                    wires.insert(cmd.target_wire, match cmd.gate {
                        Gate::Assign => signal_numbers[0],
                        Gate::And => signal_numbers[0] & signal_numbers[1],
                        Gate::Or => signal_numbers[0] | signal_numbers[1],
                        Gate::Not => !signal_numbers[0],
                        Gate::LShift => signal_numbers[0] << signal_numbers[1],
                        Gate::RShift => signal_numbers[0] >> signal_numbers[1],
                    });

                    executed.insert(cmd, true);
                } else {
                    executed.insert(cmd, false);
                }
            }
        }
    }

    wires["a"]
}

pub fn both_parts() {
    match helper::InputFile::read("src/day7/input7.txt") {
        Err(e) => println!("Error: {}", e),
        Ok(input_file) => {
            let lines = input_file.get_content_as_lines();
            
            let mut commands = Vec::new();

            for line in &lines {
                let mut cmd_parts: Vec<&str> = line.split_whitespace().filter(|&x| x != "->").collect();

                if !cmd_parts.is_empty() {
                    let gate = {
                        if cmd_parts.contains(&"AND") { Gate::And }
                        else if cmd_parts.contains(&"OR") { Gate::Or }
                        else if cmd_parts.contains(&"NOT") { Gate::Not }
                        else if cmd_parts.contains(&"LSHIFT") { Gate::LShift }
                        else if cmd_parts.contains(&"RSHIFT") { Gate::RShift }
                        else { Gate::Assign }
                    };

                    let target_wire = cmd_parts.pop().unwrap();

                    let source_signals: Vec<Signal> = cmd_parts.iter().filter(|s| !s.contains(char::is_uppercase)).map(|s| {
                        match s.parse::<u16>() {
                            Ok(num) => Signal::Number(num),
                            Err(_) => Signal::WireID(s),
                        }
                    }).collect();

                    commands.push(Command { gate, target_wire, source_signals });
                }
            }

            let wire_a_part1 = get_wire_a_signal(&commands);

            println!("Signal of wire a (part 1): {}", wire_a_part1);

            for cmd in &mut commands {
                if cmd.target_wire == "b" && cmd.gate == Gate::Assign {
                    cmd.source_signals = vec![Signal::Number(wire_a_part1)];
                    break;
                }
            }
            
            println!("Signal of wire a (part 2): {}", get_wire_a_signal(&commands));
        },
    }
}