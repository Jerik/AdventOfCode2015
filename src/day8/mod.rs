use helper;

pub fn both_parts() {
    match helper::InputFile::read("src/day8/input8.txt") {
        Err(e) => println!("Error: {}", e),
        Ok(input_file) => {
            let strings = input_file.get_content_as_lines();
            
            let mut difference_part1 = 0;
            let mut difference_part2 = 0;

            for string in &strings {
                if !string.is_empty() {
                    let escaped_str = string.replace("\\\\", "a").replace("\\\"", "a");
                    difference_part1 += 2 + string.len() - escaped_str.len() + escaped_str.matches("\\x").count() * 3;
                    difference_part2 += 2 + string.matches('\"').count() + string.matches('\\').count();
                }
            }

            println!("There are {} more characters in the code.", difference_part1);
            println!("There are {} more characters in the newly encoded strings.", difference_part2);
        },
    }
}