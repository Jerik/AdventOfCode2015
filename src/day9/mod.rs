use helper;
use std::collections::HashMap;

pub fn both_parts() {
    match helper::InputFile::read("src/day9/input9.txt") {
        Err(e) => println!("Error: {}", e),
        Ok(input_file) => {
            let lines = input_file.get_content_as_lines();

            let mut locations: Vec<&str> = Vec::new();
            let mut distances: HashMap<(&str, &str), u32> = HashMap::new();

            for line in &lines {
                let distance_info: Vec<&str> = line.split_whitespace()
                                                   .filter(|&s| s != "to" && s != "=")
                                                   .collect();

                if distance_info.len() == 3 {
                    for loc in &distance_info[0..2] {
                        if !locations.contains(loc) {
                            locations.push(loc);
                        }
                    }

                    let distance = distance_info[2].parse().unwrap();
                    distances.insert((distance_info[0], distance_info[1]), distance);
                    distances.insert((distance_info[1], distance_info[0]), distance);
                }
            }

            let route_lengths: Vec<u32> = helper::permutations(&locations).iter().map(|perm| {
                perm.windows(2).map(|locs| distances[&(locs[0], locs[1])]).sum::<u32>()
            }).collect();

            println!("Shortest distance: {}", route_lengths.iter().min().unwrap());
            println!("Longest distance: {}", route_lengths.iter().max().unwrap());
        },
    }
}