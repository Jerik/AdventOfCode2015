use std::io;
use std::io::prelude::*;
use std::fs::File;

pub const ALPHABET: &str = "abcdefghijklmnopqrstuvwxyz";

pub struct InputFile {
    content: String,
}

impl InputFile {
    pub fn read(filepath: &str) -> io::Result<InputFile> {
        let mut file = File::open(filepath)?;

        let mut content = String::new();
        file.read_to_string(&mut content)?;

        Ok(InputFile { content })
    }

    pub fn get_content(&self) -> String {
        self.content.to_string()
    }

    pub fn get_content_as_lines(&self) -> Vec<String> {
        self.content.lines().map(|x| x.to_string()).collect()
    }
}

pub fn permutations<T: Clone>(collection: &[T]) -> Vec<Vec<T>> {
    if collection.is_empty() {
        vec![vec![]]
    } else {
        collection.iter().enumerate().flat_map(|(i, elem)| {
            let other_elems: Vec<T> = collection[..i].iter().chain(&collection[i+1..]).cloned().collect();
            permutations(&other_elems).iter().map(|perm| {
                [elem.clone()].iter().chain(perm).cloned().collect()
            }).collect::<Vec<Vec<T>>>()
        }).collect()
    }
}

pub fn sized_combinations<T: Clone>(collection: &[T], n: usize) -> Vec<Vec<T>> {
    if collection.is_empty() || n == 0 {
        vec![vec![]]
    } else if n >= collection.len() {
        vec![collection.to_vec()]
    } else {
        collection[..collection.len()-n+1].iter().enumerate().flat_map(|(i, elem)| {
            sized_combinations(&collection[i+1..], n-1).iter().map(|comb| {
                [elem.clone()].iter().chain(comb).cloned().collect()
            }).collect::<Vec<Vec<T>>>()
        }).collect()
    }
}

pub fn combinations<T: Clone>(collection: &[T]) -> Vec<Vec<T>> {
    (0..collection.len()+1).flat_map(|n| {
        sized_combinations(collection, n)
    }).collect()
}

/// Counts non-overlapping occurences of pattern p in string s
pub fn count_pattern(s: &str, p: &str) -> usize {
    if p.is_empty() {
        0
    } else {
        (s.len() - s.replace(p, "").len()) / p.len()
    }
}

pub fn get_difference<T: Clone + PartialEq>(collection1: &[T], collection2: &[T]) -> Vec<T> {
    let mut difference = collection1.to_vec();

    for elem in collection2 {
        if let Some(i) = difference.iter().position(|x| x == elem) {
            difference.remove(i);
        }
    }

    difference
}

/// Returns all possible combinations of p summands (with 0 <= summand <= n), so that their sum is n
pub fn nwps(n: usize, p: usize) -> Vec<Vec<usize>> {
    if p == 1 {
        vec![vec![n]]
    } else {
        (0..n+1).flat_map(|i| {
            nwps(n-i, p-1).iter().map(|x| {
                [i].iter().chain(x).cloned().collect()
            }).collect::<Vec<Vec<usize>>>()
        }).collect()
    }
}