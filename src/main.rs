#![allow(unknown_lints)]

extern crate md5;
extern crate serde_json;

mod helper;
mod day1;
mod day2;
mod day3;
mod day4;
mod day5;
mod day6;
mod day7;
mod day8;
mod day9;
mod day10;
mod day11;
mod day12;
mod day13;
mod day14;
mod day15;
mod day16;
mod day17;
mod day18;
mod day19;
mod day20;
mod day21;
mod day22;
mod day23;
mod day24;
mod day25;

fn main() {
    println!("Day  1:"); day1::both_parts(); println!();
    println!("Day  2:"); day2::both_parts(); println!();
    println!("Day  3:"); day3::both_parts(); println!();
    println!("Day  4:"); day4::both_parts(); println!();
    println!("Day  5:"); day5::both_parts(); println!();
    println!("Day  6:"); day6::both_parts(); println!();
    println!("Day  7:"); day7::both_parts(); println!();
    println!("Day  8:"); day8::both_parts(); println!();
    println!("Day  9:"); day9::both_parts(); println!();
    println!("Day 10:"); day10::both_parts(); println!();
    println!("Day 11:"); day11::both_parts(); println!();
    println!("Day 12:"); day12::both_parts(); println!();
    println!("Day 13:"); day13::both_parts(); println!();
    println!("Day 14:"); day14::both_parts(); println!();
    println!("Day 15:"); day15::both_parts(); println!();
    println!("Day 16:"); day16::both_parts(); println!();
    println!("Day 17:"); day17::both_parts(); println!();
    println!("Day 18:"); day18::both_parts(); println!();
    println!("Day 19:"); day19::both_parts(); println!();
    println!("Day 20:"); day20::both_parts(); println!();
    println!("Day 21:"); day21::both_parts(); println!();
    println!("Day 22:"); day22::both_parts(); println!();
    println!("Day 23:"); day23::both_parts(); println!();
    println!("Day 24:"); day24::both_parts(); println!();
    println!("Day 25:"); day25::part1();
}